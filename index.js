var express = require("express");
var app = express();

app.set('view engine', 'jade');
app.set('views', __dirname + '/views');

var users = [
  {
    id: 0,
    name: 'Hosy',
    age: '22',
    hobby: 'Slot'
  },
  {
    id: 1,
    name: 'Endoh',
    age: '25',
    hobby: null
  },
  {
    id: 2,
    name: 'Kichy',
    age: '12',
    hobby: 'のぞき'
  }
];

app.get('/', function(req, res, next){
  res.render('index', {
    users: users
  });
});

app.get("/users/:id", function(req, res, next){
  res.render('users/index', {
    user: users[req.params.id]
  });
});
// 
// app.get("/users/show", function(req, res, next){
//   res.json(users);
// });

app.get('/js/:file_name', function(req, res, next){
  res.sendFile(__dirname + '/js/' + req.params.file_name);
});

app.get('/css/:file_name', function(req, res, next){
  res.sendFile(__dirname + '/css/' + req.params.file_name);
});

var server = app.listen(3000, function(){
    console.log("Node.js is listening to PORT:" + server.address().port);
});
